**VirusTotalPy3 - v2**
Python3 client of VirusTotal API v2

**Basic usage**


```
__VT_API_KEY__ = "blahh"
vt = VirusTotal(__VT_API_KEY__)
vt.file_report("84c82835a5d21bbcf75a61706d8ab549")
print(vt.scans)
```
