__author__ = "Gonzalo Villegas a.k.a Cl34r"
import requests


class VirusTotal(object):
    def __init__(self, apikey):
        self.apikey = apikey
        self.URL_BASE = "https://www.virustotal.com/vtapi/v2/"
        self.HTTP_OK = 200
        self.params = {'apikey': self.apikey}

        #  VARIABLES IP REPORT
        self.as_owner = ""
        self.continent = ""
        self.country = ""
        self.asn = ""
        self.detected_communicating_samples = []
        self.detected_downloaded_samples = []
        self.detected_referrer_samples = []
        self.detected_urls = []
        self.network = ""
        self.resolutions = []
        self.response_code = ""
        self.undetected_communicating_samples = []
        self.undetected_downloaded_samples = []
        self.undetected_referrer_samples = []
        self.undetected_urls = []
        self.whois = ""
        #  VARIABLES DNS_REPORT
        self.subdomains = []
        self.categories = []
        self.domain_siblings = []
        #  URL REPORT
        self.verbose_msg = ""
        self.scan_id = ""
        self.permalink = ""
        self.url = ""
        self.scan_date = ""
        self.filescan_id = ""
        self.positives = ""
        self.total = 0
        self.scans = []
        #  FILE SCAN
        self.resource = ""
        self.md5 = ""
        self.sha1 = ""
        self.sha256 = ""
        self.total = ""
        self.scans = ""

    def reset(self):
        self.__init__(self.apikey)

    def print_full_report(self):
        pass

    def ip_report(self, ip):
        """
        Se indica IP como parametro para obtener reporte
        :param ip:
        :return verbose_msg:
        :return as_owner:
        :return continent:
        :return country:
        :return asn:
        :return detected_communicating_samples:
        :return detected_downloaded_samples:
        :return detected_referrer_samples:
        :return detected_urls:
        :return network:
        :return resolutions:
        :return response_code:
        :return undetected_communicating_samples:
        :return undetected_downloaded_samples:
        :return undetected_referrer_samples:
        :return undetected_urls:
        :return whois:
        """
        endpoint = "ip-address/report"
        params = self.params
        params.update({"ip": ip})
        try:
            r = requests.get(self.URL_BASE + endpoint, params=params)
            if r.status_code == self.HTTP_OK:
                if r.json()["response_code"] == 1:
                    if "verbose_msg" in r.json(): self.verbose_msg = r.json()["verbose_msg"]
                    if "as_owner" in r.json(): self.as_owner = r.json()["as_owner"]
                    if "continent" in r.json(): self.continent = r.json()["continent"]
                    if "country" in r.json(): self.country = r.json()["country"]
                    if "asn" in r.json(): self.asn = r.json()["asn"]
                    if "detected_communicating_samples" in r.json(): self.detected_communicating_samples = r.json()["detected_communicating_samples"]
                    if "detected_downloaded_samples" in r.json(): self.detected_downloaded_samples = r.json()["detected_downloaded_samples"]
                    if "detected_referrer_samples" in r.json(): self.detected_referrer_samples = r.json()["detected_referrer_samples"]
                    if "detected_urls" in r.json(): self.detected_urls = r.json()["detected_urls"]
                    if "network" in r.json(): self.network = r.json()["network"]
                    if "resolutions" in r.json(): self.resolutions = r.json()["resolutions"]
                    if "response_code" in r.json(): self.response_code = r.json()["response_code"]
                    if "undetected_communicating_samples" in r.json(): self.undetected_communicating_samples = r.json()["undetected_communicating_samples"]
                    if "undetected_downloaded_samples" in r.json(): self.undetected_downloaded_samples = r.json()["undetected_downloaded_samples"]
                    if "undetected_referrer_samples" in r.json(): self.undetected_referrer_samples = r.json()["undetected_referrer_samples"]
                    if "undetected_urls" in r.json(): self.undetected_urls = r.json()["undetected_urls"]
                    if "whois" in r.json(): self.whois = r.json()["whois"]
                if r.json()["response_code"] != 1:
                    raise NameError(r.json()["verbose_msg"])
        except requests.exceptions.Timeout:
            print("[-] VT API: Timeout")
            exit(1)
        except requests.exceptions.TooManyRedirects:
            print("[-] VT API: Too many redirects")
            exit(1)
        except requests.exceptions.RequestException as e:
            print("[-] VT API: No handled error")
            raise SystemExit(e)
        except Exception as e:
            print("[-] VT API: ", e)
            raise

    def url_report(self, resource, allinfo="false", scan="0"):
        """
        Se utiliza para obtener informacion de una URL ya escaneada o obtener un reporte especifico por su
        scan_id como 'resource' accediendo al reporte especifico.
        Parametro opcional 'allinfo' entrega resultado verbose.
        Parametro opcional 'scan' valor 1 escanea el 'resource' si es que este no ha sido escaneado antes, entregando
        un 'scan_id' para realizar consulta mas tarde en busqueda del reporte. Valor 0 por defecto no escanea.
        :param resource:
        :param allinfo:
        :param scan:
        :return verbose_msg:
        :return scan_id:
        :return permalink:
        :return url:
        :return scan_date:
        :return filescan_id:
        :return positives:
        :return total:
        :return scans:
        """
        endpoint = "url/report"
        params = self.params
        params.update({"resource": resource})
        params.update({"scan": scan})
        params.update({"allinfo": allinfo})
        try:
            r = requests.get(self.URL_BASE + endpoint, params=params)
            if r.status_code == self.HTTP_OK:
                if r.json()["response_code"] == 1:
                    if "verbose_msg" in r.json(): self.verbose_msg = r.json()["verbose_msg"]
                    if "scan_id" in r.json(): self.scan_id = r.json()["scan_id"]
                    if "permalink" in r.json(): self.permalink = r.json()["permalink"]
                    if "url" in r.json(): self.url = r.json()["url"]
                    if "scan_date" in r.json(): self.scan_date = r.json()["scan_date"]
                    if "filescan_id" in r.json(): self.filescan_id = r.json()["filescan_id"]
                    if "positives" in r.json(): self.positives = r.json()["positives"]
                    if "total" in r.json(): self.total = r.json()["total"]
                    if "scans" in r.json(): self.scans = r.json()["scans"]
                if r.json()["response_code"] != 1:
                    raise NameError(r.json()["verbose_msg"])

        except requests.exceptions.Timeout:
            print("[-] VT API: Timeout")
            exit(1)

        except requests.exceptions.TooManyRedirects:
            print("[-] VT API: Too many redirects")
            exit(1)

        except requests.exceptions.RequestException as e:
            print("[-] VT API: No handled error")
            raise SystemExit(e)

    def domain_report(self, domain):
        """
        Se indica el dominio como parametro para obtener reporte
        :param domain:
        :return verbose_msg:
        :return undetected_referrer_samples:
        :return detected_downloaded_samples:
        :return detected_referrer_samples:
        :return undetected_downloaded_samples:
        :return resolutions:
        :return subdomains:
        :return categories:
        :return domain_siblings:
        :return undetected_urls:
        :return detected_urls:
        """
        endpoint = "domain/report"
        params = self.params
        params.update({"domain": domain})
        try:
            r = requests.get(self.URL_BASE + endpoint, params=params)
            if r.status_code == self.HTTP_OK:
                if r.json()["response_code"] == 1:
                    if "verbose_msg" in r.json(): self.verbose_msg = r.json()["verbose_msg"]
                    if "undetected_referrer_samples" in r.json(): self.undetected_referrer_samples = r.json()["undetected_referrer_samples"]
                    if "detected_downloaded_samples" in r.json(): self.detected_downloaded_samples = r.json()["detected_downloaded_samples"]
                    if "detected_referrer_samples" in r.json(): self.detected_referrer_samples = r.json()["detected_referrer_samples"]
                    if "undetected_downloaded_samples" in r.json(): self.undetected_downloaded_samples = r.json()["undetected_downloaded_samples"]
                    if "resolutions" in r.json(): self.resolutions = r.json()["resolutions"]
                    if "subdomains" in r.json(): self.subdomains = r.json()["subdomains"]
                    if "categories" in r.json(): self.categories = r.json()["categories"]
                    if "domain_siblings" in r.json(): self.domain_siblings = r.json()["domain_siblings"]
                    if "undetected_urls" in r.json(): self.undetected_urls = r.json()["undetected_urls"]
                    if "detected_urls" in r.json(): self.detected_urls = r.json()["detected_urls"]

                if r.json()["response_code"] != 1:
                    raise NameError(r.json()["verbose_msg"])

        except requests.exceptions.Timeout:
            print("[-] VT API: Timeout")
            exit(1)

        except requests.exceptions.TooManyRedirects:
            print("[-] VT API: Too many redirects")
            exit(1)

        except requests.exceptions.RequestException as e:
            print("[-] VT API: No handled error")
            raise SystemExit(e)

    def file_report(self, resource, allinfo="false"):
        """
                Se utiliza para obtener informacion de un archivo ya escaneado u obtener un reporte especifico por su
                scan_id como 'resource' accediendo al reporte especifico.
                Parametro opcional 'allinfo' entrega resultado verbose.
                :param resource:
                :param allinfo:
                :return verbose_msg:
                :return response_code:
                :return resource:
                :return scan_id:
                :return md5:
                :return sha1:
                :return sha256:
                :return scan_date:
                :return permalink:
                :return positives:
                :return total:
                :return scans:
                """
        endpoint = "file/report"
        params = self.params
        params.update({"resource": resource})
        params.update({"allinfo": allinfo})
        try:
            r = requests.get(self.URL_BASE + endpoint, params=params)
            if r.status_code == self.HTTP_OK:
                if r.json()["response_code"] == 1:
                    if "verbose_msg" in r.json(): self.verbose_msg = r.json()["verbose_msg"]
                    if "response_code" in r.json(): self.response_code = r.json()["response_code"]
                    if "resource" in r.json(): self.resource = r.json()["resource"]
                    if "scan_id" in r.json(): self.scan_id = r.json()["scan_id"]
                    if "md5" in r.json(): self.md5 = r.json()["md5"]
                    if "sha1" in r.json(): self.sha1 = r.json()["sha1"]
                    if "sha256" in r.json(): self.sha256 = r.json()["sha256"]
                    if "scan_date" in r.json(): self.scan_date = r.json()["scan_date"]
                    if "permalink" in r.json(): self.permalink = r.json()["permalink"]
                    if "positives" in r.json(): self.positives = r.json()["positives"]
                    if "total" in r.json(): self.total = r.json()["total"]
                    if "scans" in r.json(): self.scans = r.json()["scans"]

                if r.json()["response_code"] != 1:
                    raise NameError(r.json()["verbose_msg"])

        except requests.exceptions.Timeout:
            print("[-] VT API: Timeout")
            exit(1)

        except requests.exceptions.TooManyRedirects:
            print("[-] VT API: Too many redirects")
            exit(1)

        except requests.exceptions.RequestException as e:
            print("[-] VT API: No handled error")
            raise SystemExit(e)

    def url_scan(self, url):
        """
            Se indica URL como parametro para obtener reporte
            :param url:
            :return verbose_msg:
            :return permalink:
            :return resource:
            :return url:
            :return scan_date:
            :return scan_id:
        """
        endpoint = "url/scan"
        params = self.params
        params.update({"url": url})
        try:
            r = requests.post(self.URL_BASE + endpoint, params=params)
            if r.status_code == self.HTTP_OK:
                print(r.json())
                if r.json()["response_code"] == 1:
                    if "verbose_msg" in r.json(): self.verbose_msg = r.json()["verbose_msg"]
                    if "permalink" in r.json(): self.permalink = r.json()["permalink"]
                    if "resource" in r.json(): self.resource = r.json()["resource"]
                    if "url" in r.json(): self.url = r.json()["url"]
                    if "scan_date" in r.json(): self.scan_date = r.json()["scan_date"]
                    if "scan_id" in r.json(): self.scan_id = r.json()["scan_id"]

                if r.json()["response_code"] != 1:
                    raise NameError(r.json()["verbose_msg"])
        except requests.exceptions.Timeout:
            print("[-] VT API: Timeout")
            exit(1)
        except requests.exceptions.TooManyRedirects:
            print("[-] VT API: Too many redirects")
            exit(1)
        except requests.exceptions.RequestException as e:
            print("[-] VT API: No handled error")
            raise SystemExit(e)
        except Exception as e:
            print("[-] VT API: ", e)
            raise SystemExit(e)